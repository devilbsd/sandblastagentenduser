.. Proteccion del Endpoint con SandBlast Agent documentation master file, created by
   sphinx-quickstart on Tue Dec  5 18:41:13 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Proteccion del Endpoint con Check Point SandBlast Agent
=======================================================

.. toctree::
   :maxdepth: 2
   :caption: Contenido:
   :numbered:
   :titlesonly:
   :glob:
   :hidden:

    lab01.rst

Acerca de este documento
========================

Este documento describe los ejercicios a realizar para proteger un equipo "Endpoint" utilizando un marco que contempla 5 capas de proteccion.

* Cumplimiento de regulaciones (Compliance).
* Control de vectores.
* Prevencion de ataques.
* Deteccion y mitigacion de ataques.
* Remediacion de ataques.


Detalles del ambiente de laboratorio
====================================

El ambiente para este entrenamiento consta de 5 máquinas virtuales:

- IntGateway: Equipo perimetral que suministra de Internet al ambiente, protege la red de usuarios finales y de administración. **Plataforma: Check Point Securtity Gateway R77.30.**
- Managemnet: Gestiona las políticas de seguridad y las bitácoras de el 'gateway' de perimetro y los agentes de SandBlast. **Plataforma: Check Point SmartCenter R77.30.03.**
- Windows 7: Equipo de un usuario final.
- Windows 10: Equipo del Administrador de la plataforma. Todas las herramientas de gestión estan instaladas en este equipo.


Usuarios y passwords
^^^^^^^^^^^^^^^^^^^^


+---------------+------------------+--------------+----------+ 
| VM            | IP               | User         | Password |
+===============+==================+==============+==========+ 
| IntGateway    | 192.168.100.254  | admin/expert | vpn123   | 
+---------------+------------------+--------------+----------+ 
| Management    | 192.168.100.100  | admin/expert | vpn123   | 
+---------------+------------------+--------------+----------+ 
| SmartConsole  | @Windows 10 VM   | admin        | vpn123   |
+---------------+------------------+--------------+----------+ 
| Windows 7     | 192.168.50.50    | user01       | vpn123   | 
+---------------+------------------+--------------+----------+
| Windows 10    | 192.168.50.70    | usermgmt     | vpn123   |
+---------------+------------------+--------------+----------+


Tabla NATs y puertos publicados
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


+---------------+------------------+------------------+---------------------+----------------------+ 
| VM            | Servicio         | IP:Port WAN      | IP:Port CheckPoint  | IP:Port VM           |
+===============+==================+==================+=====================+======================+ 
| AppGateway    | SSH              | <WAN-DHCP>:22    | No Aplica           | <WAN-DHCP>:22        |
+---------------+------------------+------------------+---------------------+----------------------+ 
| AppGateway    | HTTP             | <WAN-DHCP>:80    | No Aplica           | <WAN-DHCP>:80        |
+---------------+------------------+------------------+---------------------+----------------------+ 
| IntGateway    | SSH              | <WAN-DHCP>:10022 | 10.10.10.100:22     | 10.10.10.100:22      |
+---------------+------------------+------------------+---------------------+----------------------+ 
| IntGateway    | HTTPS GAiA       | <WAN-DHCP>:10443 | 10.10.10.100:443    | 10.10.10.100:443     | 
+---------------+------------------+------------------+---------------------+----------------------+ 
| Management    | SSH              | <WAN-DHCP>:6022  | 10.10.10.60:6022    | 192.168.100.100:22   |
+---------------+------------------+------------------+---------------------+----------------------+ 
| Management    | HTTPS GAiA       | <WAN-DHCP>:64434 | 10.10.10.60:64434   | 192.168.100.100:4434 |
+---------------+------------------+------------------+---------------------+----------------------+ 
| Management    | CPMI-SmartConsole| <WAN-DHCP>:18190 | 10.10.10.60:18190   | 192.168.100.100:18190|
+---------------+------------------+------------------+---------------------+----------------------+
| Windows10Mgmt | Remote Desktop   | <WAN-DHCP>:23389 | 10.10.10.20:3389    | 192.168.100.50:3389  |
+---------------+------------------+------------------+---------------------+----------------------+ 
| Windows7User01| Remote Desktop   | <WAN-DHCP>:53389 | 10.10.10.50:3389    | 192.168.50.50:3389   |
+---------------+------------------+------------------+---------------------+----------------------+ 






Lab #1. Prevención: Compliance, Firewall & App Control.
==================================================================

Objetivo del laboratorio
^^^^^^^^^^^^^^^^^^^^^^^^^^





Lab #2. Prevención: Sanitización de documentos con Threat Extraction.
======================================================================

Objetivo del laboratorio
^^^^^^^^^^^^^^^^^^^^^^^^^^





Lab #3. Prevención: Zero-Phishing.
======================================================================

Objetivo del laboratorio
^^^^^^^^^^^^^^^^^^^^^^^^^^





Lab #4. Detección & Mitigacion: Threat Emulation @Cloud & Forensics.
======================================================================

Objetivo del laboratorio
^^^^^^^^^^^^^^^^^^^^^^^^^^


Lab #5. Integración de SandBlast Agent con SandBlast TE100X.
======================================================================

Objetivo del laboratorio
^^^^^^^^^^^^^^^^^^^^^^^^^^

Enviar todos los archivos por analizar a un equipo *SandBlast TE100X*



Lab #6. Detección: ¿Qué sucede dentro del *SandBox*?.
======================================================================

Objetivo del laboratorio
^^^^^^^^^^^^^^^^^^^^^^^^^^

Mostrar en tiempo real el comportamiento de algunos programas maliciosos mientras son ejecutados en el *SandBox*. En esta sección el usuario utilizará un portal Web paraa enviar diferentes muestras de malware en diferentes formatos, 



Lab #7. Remediación: Forensics & Anti-Ransomware.
======================================================================

Objetivo del laboratorio
^^^^^^^^^^^^^^^^^^^^^^^^^^

Mostrar las capacidades de *SandBlast Agent* para detener el ataque de un *Ransomware* y recuperar la información encriptada.
Antes de comenzar la ejecición del malware, se debn cumplir los siguientes putnos:

#. Verificar que Windows Defender no esta activo.
#. Deshabilitar el análisis por parte de Threat Emulation







Opcional - Lab #8. Instalacion y actualizacion de agentes.
================================================


Objetivo del laboratorio.
^^^^^^^^^^^^^^^^^^^^^^^^^

En esta seccion se implementa *SandBlast Agent* en el equipo de computo de un usuario final, ambos agentes instalados utilizan politicas diferentes que garantizan la continuidad del servcio. Aunque se realiza la configuración de diferentes Software Blades, en este laboratorio solo se instala el módulo de "Compliance" en el equipo Windows 7 con el propótiso de demostrar la modularidad de solución.

Al finalizar el laboratorio ambos equipos windows deben utilizar la versión más reciente del agente ( E80.72).


Estado inicial del laboratorio.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

a. EP management tiene los clientes version **E80.71HF1**. SBA e *Initial Client*. Politicas predefinidas para el grupo de *Management*, el usuario debe generar las propias para el grupo virtual *Users*.

b. Windows 7 User, tiene actualizaciones pero no tiene ningun agente instalado. No hay herramientas de gestion. El usuario final debe instalar el agente inicial en version E80.71 HF1 y actualizar hasta E80.72

c. Windows 10 Mgmt, tiene actualizaciones y **SandBlast Agent** completo instalado, el usuario debe de actualizar tambien este agente a **E80.72**.


Configuraciones iniciales.
^^^^^^^^^^^^^^^^^^^^^^^^^^

#. Simplificar la vista para el tab de politicas, mostrando solamente los productos que comprenden SBA.
#. Modificar dentro de la seccion *Client Settings* los siguientes parametros:
   a. Log Upload > Log upload interval: de 20 min a 1 min
   a. Log Upload > Maximun number of events before attempting an upload: de 10 min a 1 min
#. Crear un para exportar en *Packages for Export* para el grupo cirtual **Users**, que instale loa mósuloa de **Compliance**, **Firewall**, y **Control de Aplicaciones.**
#. Crear una regla en el tab de *Deployment* para el grupo virtual **Users**, la regla solo instala **Compliance**, **Firewall**, y **Control de Aplicaciones.*** en el agente.
#. Salvar e instalar politica en EPM, deben reflejarse cambios en la secciones *Client Settings* y *Software Deployment*.


Instalacion del agente inicial.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#. Exportar el agente inicial version E80.71.0232 y asignarlo al Virtual Group "Users". Seleccionar *Desktop* como destino de la descarga, el archivo **EPS.msi** de 14 MB se creara en el escritorio. **NO RENOMBRAR EL ARCHIVO**.
#. Subir el archivo EPS.msi a PooMA.
#. Descargar de PooMA el archivo EPS.msi en el equipo Windows 7.
#. Abrir cmd con privilegios de administrador y cambiarse a la carpeta de **Downloads**.
#. Lanzar EPS.msi con la siguiente linea de comando para reaizar una instalacion desatendida:
::
   msiexec  /i EPS.msi /l*v instal.log /qb!
#. Monitorear el proceso de instalacion en la seccion de reportes de **SmartEndpoint** y en los pop-ups que lanza el agente.
#. Al terminar la instalacion la VM reiniciara y el agente instalado debe reflejar solo el módulo de compliance instalado y la version correcta. 
#. Confirmar los siguientes puntos:
      - Esta el cliente conectado a la consola?
      - Que version esta instalada?
      - Que modulos tiene instalados?
      - Que politicas está utilizando el agente?
#. Abrir Process Explorer y verificar que procesos pertenecen a Check Point SandBlast y cual es su consumo en CPU y RAM.

Actualizacion de todos los agentes a E80.72
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. Importar los clientes E80.72 a SmartEndpoint, Initial Client, SBAgent 64bits y 32 bits.
#. Modificar la politica de deployment para utilizar la nueva verion de los agentes, tanto para *Management* como para *Users*.
#. Salvar e instalar politica.
#. Monitorea el proceso de instalacion en la seccion de reportes de **SmartEndpoint** y en los pop-ups que lanza el agente.
#. Al terminar la instalacion la VM reiniciara y el agente instalado debe reflejar solo el módulo de compliance instalado y la version correcta. 
#. Confirmar los siguientes puntos:
      - Esta el cliente conectado a la consola?
      - Que version esta instalada?
      - Que modulos tiene instalados?
      - Que politicas está utilizando el agente?




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
